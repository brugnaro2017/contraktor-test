## Instruções para iniciar o projeto local:

> git clone git@gitlab.com:brugnaro2017/contraktor-test.git

> cd backend

> yarn

> yarn start

> cd ..

> cd client

> yarn

> yarn start

> Acessar url: http://localhost:5000/client

## Instruções para iniciar o projeto em produção:

> Acessar url: https://brugnaro.github.io/client/

## Informações sobre estruturas:

> Banco de dados: conexão com mongodb mlab

> Backend: Express, Mongoose, Apollo-server-express, Graphql-tools

> Client: Apollo-boost, React-apollo