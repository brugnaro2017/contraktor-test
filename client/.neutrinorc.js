module.exports = {
  use: [
    '@neutrinojs/airbnb',
    [
      '@neutrinojs/react'
    ],
    '@neutrinojs/jest',
    '@neutrinojs/eslint', {
      rules: {
        "react/jsx-filename-extension": [1, { extensions: [".js", ".jsx"] }]
      }
    }
  ]
};
