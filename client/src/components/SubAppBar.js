import React from 'react';

export default props => {
  return (
    <div className='subappbar-container'>
      {props.children}
    </div>
  )
};
