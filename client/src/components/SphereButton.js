import React from 'react';
import Icon from '../common/Icon';

export default props => {
  return (
    <div onClick={props.onClick} className='sphere-btn'>
      <Icon color='#3BAADF' name={props.icon} size={props.iconSize} />
      <div className='sphere-btn-title'>
        <p>{props.title}</p>
      </div>
    </div>
  );
};
