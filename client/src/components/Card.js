import React from 'react';

export default props => (
  <div className='card-container'>
    <div className='card-content'>
      <h4>{props.title}</h4>
      {props.children}
    </div>
  </div>
);