import React from 'react';
import Icon from '../common/Icon';

export default props => {
  return (
    <div className='icon-route-bar' onClick={props.onClick}>
      <Icon color='#3BAADF' name='&#8617;' size={30} />
    </div>
  );
};
