import React from 'react';

export default props => (
  <button
    type={props.type}
    disabled={props.disabled}
    style={{ backgroundColor: !props.colorButton ? '#3498db' : '#DDD' }}
  >
    {props.name}
  </button>
);