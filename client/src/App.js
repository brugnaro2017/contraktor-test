import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import UserContainer from './screens/User/UserContainer';
import ContractContainer from './screens/Contract/ContractContainer';

import './common/common.style.css';

export default () => (
  <Router>
    <Switch>
      <Route path='/client/user' component={UserContainer} />
      <Route path='/client/contract' component={ContractContainer} />
      <Route path='/client' component={ContractContainer} />
    </Switch>
  </Router>
);
