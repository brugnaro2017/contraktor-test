import { render } from 'react-dom';
import AppContainer from './AppContainer';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import App from './App';

const uriDev = 'http://localhost/graphql';
const uriProd = 'https://back-end-ck.herokuapp.com/graphql';

const client = new ApolloClient({
  uri: uriDev
});

const root = document.getElementById('root');

const load = () => render(
  (
    <AppContainer>
      <ApolloProvider client={client}>
        <App />
      </ApolloProvider>
    </AppContainer>
  ), root,
);

if (module.hot) {
  module.hot.accept('./App', load);
}

load();
