import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import { GET_ALL_CONTRACTS, DELETE_CONTRACT } from '../../store/queries';

import Message from '../../common/Message';

import SubAppBar from '../../components/SubAppBar';
import SphereButton from '../../components/SphereButton';

import Dialog from '../../common/Dialog';

import './styles.css';

export default class ContractList extends Component {
  state = {
    search: '',
    searchInitialDate: '',
    searchDueDate: '',
    show: false,
    message: false
  };

  render() {
    const { message, search, searchInitialDate, searchDueDate, show } = this.state;
    const { data, history, handlePage } = this.props;

    const updateSearch = e => {
      this.setState({ search: e.target.value });
    };

    const updateSearchInitialDate = e => {
      this.setState({ searchInitialDate: e.target.value });
    };

    const updateSearchDueDate = e => {
      this.setState({ searchDueDate: e.target.value });
    };

    let filteredContracts = data.getAllContracts.filter(
      contract => {
        let result = {};
        if (search !== '') result = contract.title.indexOf(search) !== -1;
        if (searchInitialDate !== '') result = contract.initialDate.indexOf(searchInitialDate) !== -1;
        if (searchDueDate !== '') result = contract.dueDate.indexOf(searchDueDate) !== -1;
        return result
      }
    );

    const handleDialog = (show) => {
      this.setState({ show });
    };

    const handleDelete = (deleteContract) => {

      const confirmDelete = window.confirm('Tem certeza que deseja excluir teste contrato?');

      if (confirmDelete) {
        deleteContract()
          .then(({ data }) => {
          }).catch(err => console.log('Error', err.message));
      }

    }

    const showMessage = () => {
      this.setState({ message: true });
    }

    const hideMessage = () => {
      setTimeout(() => this.setState({ message: false }), 3000);
    }

    return (
      <div>
        <div className='message-info-container'>
          {message && <Message icon='&#10004;' title='Mensagem' message='Contrato removido com sucesso!' colorIcon='#1FA463' />}
        </div>

        <SubAppBar>
          <SphereButton onClick={() => history.push('/client/user')} title='Cadastrar Usuário' icon='&#9639;' iconSize={20} />
          <SphereButton onClick={() => handlePage('register')} title='Cadastrar Contrato' icon='&#9783;' iconSize={20} />
        </SubAppBar>

        <div className='contract-search-container'>
          <div className='input-group group-input-search'>
            <label>Buscar pelo Título</label>
            <input
              type='search'
              placeholder='Digite o título do contrato'
              onChange={(e) => updateSearch(e)}
              value={search}
            />
          </div>
          <div className='input-group'>
            <label>Buscar pela Data Inicial</label>
            <input
              type='date'
              placeholder='Digite a data inicial'
              onChange={(e) => updateSearchInitialDate(e)}
              value={searchInitialDate}
            />
          </div>
          <div className='input-group'>
            <label>Buscar pela Data de Vencimento</label>
            <input
              type='date'
              placeholder='Digite a data de vencimento'
              onChange={(e) => updateSearchDueDate(e)}
              value={searchDueDate}
            />
          </div>
        </div>

        <div className='contract-list-container'>
          <div className='contract-list-th light-bg'>
            <p>Título</p>
            <p>Data Inicial</p>
            <p>Data Vencimento</p>
            <p>Visualizar</p>
            <p>Remover</p>
          </div>

          {
            filteredContracts.map(
              contract => {
                let day = contract.initialDate.substr(8, 9);
                let month = contract.initialDate.substr(5,2);
                let year = contract.initialDate.substr(0, 4);
                let formatInitialDate = `${day}/${month}/${year}`;

                let dayDue = contract.dueDate.substr(8, 9);
                let monthDue = contract.dueDate.substr(5,2);
                let yearDue = contract.dueDate.substr(0, 4);
                let formatDueDate = `${dayDue}/${monthDue}/${yearDue}`;

                return (<div key={contract._id} className='contract-list-th'>
                  <p>{contract.title}</p>
                  <p>{formatInitialDate}</p>
                  <p>{formatDueDate}</p>
                  <p><span className='visualize-icon' onClick={() => handlePage('visualize', contract)}> &#128065;</span></p>
                  <p>
                    <Mutation
                      mutation={DELETE_CONTRACT}
                      variables={{ _id: contract._id }}
                      update={(cache, { data: { deleteContract } }) => {
                        const { getAllContracts } = cache.readQuery({
                          query: GET_ALL_CONTRACTS,
                          variables: { title: contract.title }
                        });
                        cache.writeQuery({
                          query: GET_ALL_CONTRACTS,
                          variables: { title: contract.title },
                          data: {
                            getAllContracts: getAllContracts.filter(contract => contract._id !== deleteContract._id)
                          }
                        });

                      }}
                    >
                      {
                        deleteContract => (
                          <span className='remove-icon' onClick={() => handleDelete(deleteContract)}>&#10005;</span>
                        )
                      }
                    </Mutation>
                  </p>
                </div>)
              }
            )}
        </div>
        <Dialog
          open={show}
          title='Mensagem'
          description='Deseja realmente remover o contrato?'>
          <div className='dialog-btn-container'>
            <button className='cancel-button' onClick={() => handleDialog(false)}>Cancelar</button>
            <button className='confirm-button' onClick={() => handleDelete(deleteContract, contract._id)}>Confirmar</button>
          </div>
        </Dialog>
      </div>
    )
  }
};
