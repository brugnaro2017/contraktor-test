import React from 'react';
import { Query } from 'react-apollo';
import { GET_ALL_USERS } from '../../store/queries';

import Loading from '../../common/Loading';

import Card from '../../components/Card';

import contract from '../../../assets/contract.png';

export default props => {
  const { handlePage, item } = props;
  return (
    <div className='container'>
      <h4>Visualizar Contrato</h4>
      <Query query={GET_ALL_USERS}>
        {({ data, loading, error }) => {
          if (loading) return <Loading />;
          if (error) return <div>Error</div>;
          const { getAllUsers } = data;
          return (
            <div>
              {
                getAllUsers.filter(f => f._id === item.user)
                  .map(user => {
                    let day = item.initialDate.substr(8, 9);
                    let month = item.initialDate.substr(5, 2);
                    let year = item.initialDate.substr(0, 4);
                    let formatInitialDate = `${day}/${month}/${year}`;

                    let dayDue = item.dueDate.substr(8, 9);
                    let monthDue = item.dueDate.substr(5, 2);
                    let yearDue = item.dueDate.substr(0, 4);
                    let formatDueDate = `${dayDue}/${monthDue}/${yearDue}`;
                    return (
                      <div key={user._id} className='visualize-cards'>
                        <Card title='Informações do usuário'>
                          <div className='item-card-content'>
                            <p className='title-item'>Nome:</p>
                            <p>{user.name}</p>
                          </div>
                          <div className='item-card-content'>
                            <p className='title-item'>Sobrenome:</p>
                            <p>{user.lastname}</p>
                          </div>
                          <div className='item-card-content'>
                            <p className='title-item'>Email:</p>
                            <p>{user.email}</p>
                          </div>
                          <div className='item-card-content'>
                            <p className='title-item'>Cpf:</p>
                            <p>{user.cpf}</p>
                          </div>
                          <div className='item-card-content'>
                            <p className='title-item'>Telefone:</p>
                            <p>{user.phone}</p>
                          </div>
                        </Card>
                        <Card title='Informações do contrato'>
                          <div className='item-card-content'>
                            <p className='title-item'>Título:</p>
                            <p>{item.title}</p>
                          </div>
                          <div className='item-card-content'>
                            <p className='title-item'>Data Inicial:</p>
                            <p>{formatInitialDate}</p>
                          </div>
                          <div className='item-card-content'>
                            <p className='title-item'>Data de Vencimento:</p>
                            <p>{formatDueDate}</p>
                          </div>
                          <div className='item-card-content'>
                            <p className='title-item'>Download Arquivo:</p>
                            <p>
                              <a href={item.file} download='contract.pdf'>
                                <img src={contract} width={30} />
                              </a>
                            </p>
                          </div>
                        </Card>
                      </div>
                    )
                  })
              }
            </div>
          )
        }}
      </Query>
    </div>
  );
};

