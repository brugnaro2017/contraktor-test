import React, { Component } from 'react';

import { Query, Mutation } from 'react-apollo';
import { REGISTER_CONTRACT, GET_ALL_USERS } from '../../store/queries';

import Message from '../../common/Message';
import Button from '../../components/Button';

import './styles.css';

const initialState = {
  title: '',
  initialDate: '',
  dueDate: '',
  file: '',
  user: '',
};

export default class ContractRegister extends Component {
  state = {
    ...initialState,
    save: false,
    validation: false,
  };

  clearState = () => {
    this.setState({ ...initialState });
  };

  handleChange = e => {
    let { name, value } = e.target;
    this.setState({ [name]: value });
  };

  showMessage = () => {
    this.setState({ save: true });
  }

  hideMessage = () => {
    setTimeout(() => this.setState({ save: false }), 3000);
  }

  validateForm = () => {

    const { title, initialDate, dueDate, file, user } = this.state;

    const earlyDay = initialDate.substr(8, 10);
    const earlyMonth = initialDate.substr(5, 2);
    const dayDue = dueDate.substr(8, 10);
    const monthDue = dueDate.substr(5, 2);

    const isInvalid = !title || !initialDate || !dueDate || !file || !user || initialDate === dueDate || earlyDay > dayDue || earlyMonth > monthDue;

    return isInvalid;
  }

  handleUpload = e => {
    let files = e.target.files[0];
    let reader = new FileReader();
    reader.onload = function (e) {
      getB64File(e.target.result);
    };
    reader.readAsDataURL(files);

    const getB64File = b64File => {
      this.setState({ file: b64File });
    }
  }

  handleSelectChange = e => {
    this.setState({ user: e.target.value.toString() });
  }

  handleSubmit = (e, registerContract) => {
    const { redirect } = this.props;

    e.preventDefault();

    registerContract()
      .then(data => {
        this.showMessage();
        this.clearState();
        this.hideMessage();
        setTimeout(() => redirect(), 1000);
      })
      .catch(err => console.log('Ocorreu um erro!', err.message));


  };

  render() {
    const {
      title,
      initialDate,
      dueDate,
      file,
      user,
      save,
    } = this.state;
    return (
      <div className='container'>
        <Mutation mutation={REGISTER_CONTRACT} variables={{ title, initialDate, dueDate, file, user }}>
          {(registerContract, { data, loading, error }) => {
            return (
              <form onSubmit={event => this.handleSubmit(event, registerContract)}>
                <div className='message-info-container'>
                  {
                    loading
                    && <Message icon='!' title='Informação' message='Carregando dados ...' colorIcon='#f4b914' />
                  }
                  {
                    save
                      ? <Message icon='&#10004;' title='Sucesso' message='Contrato salvo com sucesso!' colorIcon='#1FA463' />
                      : error
                        ? <Message icon='&#10006;' title='Erro' message='Erro de comunicação com servidor!' colorIcon='#D93025' />
                        : <div />
                  }
                </div>

                <h4>Cadastrar Contrato</h4>

                <div className='form-row'>
                  <div className='input-group'>
                    <label>Título</label>
                    <input type='text' name='title' value={title} onChange={this.handleChange} placeholder='Digite o título' />
                  </div>
                  <div className='input-group'>
                    <label>Data Inicial</label>
                    <input type='date' name='initialDate' value={initialDate} onChange={this.handleChange} />
                    {this.validateForm() ? <p className='message-validation'>&#9888; Data inicial deve ser anterior à data de vencimento!</p> : <p className='message-validation' />}
                  </div>
                </div>

                <div className='form-row'>
                  <div className='input-group'>
                    <label>Data de Vencimento</label>
                    <input type='date' name='dueDate' value={dueDate} onChange={this.handleChange} />
                    {this.validateForm() ? <p className='message-validation'>&#9888; Data de vencimento deve ser posterior à data inicial!</p> : <p className='message-validation' />}
                  </div>
                  <div className='input-group'>
                    <label>Selecione o usuário</label>
                    <Query query={GET_ALL_USERS}>
                      {({ data, loading, error }) => {
                        if (loading) return <div>Carregando dados...</div>;
                        if (error) return <div>Error</div>;
                        const { getAllUsers } = data;
                        return (
                          <select defaultValue={'default'} value={user} onChange={this.handleSelectChange}>
                            <option value='default' selected>Selecione o usuário</option>
                            {getAllUsers
                              .map((user, i) => (
                                <option key={i} value={user._id}>{user.name} {user.lastname}</option>
                              ))}
                          </select>
                        )
                      }}
                    </Query>
                  </div>
                </div>

                <div className='form-row'>
                  <div className='input-group input-wrapper'>
                    <label htmlFor='input-file'>Selecione o arquivo PDF do Contrato</label>
                    <input id='input-file' type='file' onChange={this.handleUpload} />
                    <span id='file-name'></span>
                  </div>
                </div>

                {
                  <Button
                    type='submit'
                    disabled={loading || this.validateForm()}
                    name='Cadastrar'
                    colorButton={this.validateForm()}
                  />
                }

              </form>
            )
          }}
        </Mutation>
      </div>
    );
  };
};
