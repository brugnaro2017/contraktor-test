import React, { Component } from 'react';
import { Query } from 'react-apollo';
import { GET_ALL_CONTRACTS } from '../../store/queries';

import Loading from '../../common/Loading';
import Message from '../../common/Message';

import AppRouteBar from '../../components/AppRouteBar';
import ContractList from './ContractList';
import ContractRegister from './ContractRegister';
import ContractVisualize from './ContractVisualize';

export default class ContractContainer extends Component {
  state = {
    page: 'list',
    item: {},
  }

  handlePage = (page, item) => {
    this.setState({ page, item });
  };

  getContracts = () => (
    <Query query={GET_ALL_CONTRACTS}>
      {({ data, loading, error }) => {
        const { history } = this.props;
        if (loading) return <Loading />;
        if (error) return (
          <div className='message-info-container'>
            <Message icon='&#10006;' title='Erro' message='Erro de comunicação com servidor!' colorIcon='#D93025' />
          </div>
        );
        return <ContractList history={history} handlePage={(page, item) => this.handlePage(page, item)} data={data} />
      }}
    </Query>
  );

  redirect = () => window.location.reload();

  render() {
    const { item, page } = this.state;

    const list = this.getContracts();
    const register = <ContractRegister handlePage={(page) => this.handlePage(page)} redirect={() => this.redirect()} />;
    const visualize = <ContractVisualize handlePage={(page) => this.handlePage(page)} item={item} />

    return (
      <div>
        {page === 'register' || page === 'visualize' ? <AppRouteBar onClick={() => this.handlePage('list')} /> : <div className='icon-route-bar' />}
        {page === 'register'
          ? register
          : page === 'visualize'
            ? visualize
            : list}
      </div>
    )
  }
};
