import React, { Component } from 'react';
import AppRouteBar from '../../components/AppRouteBar';
import UserRegister from './UserRegister';

export default class UserContainer extends Component {
  state = {
    users: {}
  }

  update = () => window.location.reload();

  render() {
    const { history } = this.props;
    return (
      <div>
        <AppRouteBar onClick={() => history.push('/client')} />
        <UserRegister history={history} update={() => this.update()} />
      </div>
    );
  };
};
