import React, { Component } from 'react';

import { Mutation } from 'react-apollo';
import { REGISTER_USER } from '../../store/queries';

import Message from '../../common/Message';
import Button from '../../components/Button';

const initialState = {
  name: '',
  lastname: '',
  email: '',
  cpf: '',
  phone: ''
};

export default class UserRegister extends Component {
  state = {
    ...initialState,
    message: '',
    save: false,
    validateCpf: false,
    errorMessage: '',
    validatePhone: false,
    messagePhone: '',
  };

  clearState = () => {
    this.setState({ ...initialState });
  }

  handleChange = e => {
    let { name, value } = e.target;
    if (name === 'cpf') value = value.replace(/\D/g, "").replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, '$1.$2.$3-$4');
    if (name === 'phone') value = value.replace(/\D/g, "").replace(/(\d{2})(\d{5})(\d{4})/g, '($1)-$2-$3')
    this.setState({ [name]: value });
  };

  validateCpf = () => {
    const { cpf, phone } = this.state;
    if (cpf.length != 14) {
      this.setState({ validateCpf: true, message: 'Digite todos os números do CPF!' });
    } else {
      this.setState({ validateCpf: false, message: '' })
    }
    if (phone.length != 15) {
      this.setState({ validatePhone: true, messagePhone: 'Digite todos os números do Telefone!' });
    } else {
      this.setState({ validatePhone: false, messagePhone: '' });
    }
  }

  handleSubmit = (e, registerUser) => {
    const { history, update } = this.props;
    const { validateCpf, validatePhone } = this.state;

    e.preventDefault();

    this.validateCpf();

    if (!validateCpf && !validatePhone) {
      registerUser()
        .then(data => {
          this.showMessage();
          this.clearState();
          this.hideMessage();
          setTimeout(() => {
            history.push('/client');
            update();
          }, 1000);
        })
        .catch(err => {
          if (err.message === 'GraphQL error: User already exists!')
            this.setState({ errorMessage: 'Usuário cadastrado anteriormente no sistema!' });
          console.log('Ocorreu um erro!', err);

        });
    }

  };

  showMessage = () => {
    this.setState({ save: true });
  }

  hideMessage = () => {
    setTimeout(() => this.setState({ save: false }), 3000);
  }

  validateForm = () => {
    const { name, lastname, email, cpf, phone } = this.state;
    const isInvalid = !name || !lastname || !email || !cpf || !phone;
    return isInvalid;
  }

  render() {
    const {
      errorMessage,
      name,
      lastname,
      email,
      cpf,
      phone,
      save,
      validateCpf,
      message,
      validatePhone,
      messagePhone,
    } = this.state;
    return (
      <div className='container'>
        <Mutation
          mutation={REGISTER_USER}
          variables={{
            name,
            lastname,
            email,
            cpf,
            phone,
          }}
        >
          {(registerUser, { data, loading, error }) => {
            return (
              <form onSubmit={e => this.handleSubmit(e, registerUser)}>
                <div className='message-info-container'>
                  {
                    loading
                    && <Message icon='!' title='Informação' message='Carregando dados ...' colorIcon='#f4b914' />
                  }
                  {
                    save
                      ? <Message icon='&#10004;' title='Sucesso' message='Contrato salvo com sucesso!' colorIcon='#1FA463' />
                      : error
                        ? <Message icon='&#10006;' title='Não foi possível cadastrar!' message={errorMessage} colorIcon='#D93025' />
                        : <div />
                  }
                </div>

                <h4>Cadastrar Usuário</h4>

                <div className='form-row'>
                  <div className='input-group'>
                    <label>Nome</label>
                    <input type='text' name='name' placeholder='Digite seu nome' onChange={this.handleChange} value={name} />
                  </div>
                  <div className='input-group'>
                    <label>Sobrenome</label>
                    <input type='text' name='lastname' placeholder='Digite seu sobrenome' onChange={this.handleChange} value={lastname} />
                  </div>
                </div>

                <div className='form-row'>
                  <div className='input-group'>
                    <label>Email</label>
                    <input type='email' name='email' placeholder='Digite seu email' onChange={this.handleChange} value={email} />
                  </div>
                  <div className='input-group'>
                    <label>Cpf</label>
                    <input type='text' name='cpf' placeholder='Digite seu cpf' onChange={this.handleChange} value={cpf} maxLength='11' />
                    {validateCpf && <p className='message-validation'>&#9888; {message}</p>}
                  </div>
                </div>

                <div className='form-row'>
                  <div className='input-group'>
                    <label>Telefone</label>
                    <input type='text' name='phone' placeholder='Digite seu número de telefone' onChange={this.handleChange} value={phone} maxLength='11' />
                    {validatePhone && <p className='message-validation'>&#9888; {messagePhone}</p>}
                  </div>
                </div>
                {
                  <Button
                    type='submit'
                    disabled={loading || this.validateForm()}
                    name='Cadastrar'
                    colorButton={this.validateForm()}
                  />
                }

              </form>
            )
          }}
        </Mutation>
      </div>
    )
  }
};
