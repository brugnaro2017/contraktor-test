import React from 'react';
import AppBar from '../src/common/AppBar';

export default props => (
  <div>
    <AppBar />
    {props.children}
  </div>
);
