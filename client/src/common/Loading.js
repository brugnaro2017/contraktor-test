import React from 'react';

export default () => (
  <div className='loading-container'>
    <div className="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
  </div>

);
