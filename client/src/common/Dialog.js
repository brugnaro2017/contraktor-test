import React from 'react';

export default props =>
  !props.open ? null :
    <div className='backdrop'>
      <div className='modal'>
        <p>{props.title}</p>
        <p>{props.description}</p>
        {props.children}
      </div>
    </div>;