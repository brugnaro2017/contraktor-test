import React from 'react';

export default props => {
  return (
    <p style={{ color: props.color, fontSize: props.size }}>{props.name}</p>
  );
};