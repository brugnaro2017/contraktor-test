import React from 'react';

import logo from '../../assets/contraktor-logo.png';

export default () => {
  return (
    <div className='app-bar'>
      <img src={logo} />
    </div>
  )
};
