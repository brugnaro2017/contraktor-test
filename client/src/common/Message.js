import React from 'react';

export default props => (
  <div className='message-info'>
    <p style={{ color: props.colorIcon, margin: '0px' }}>{props.icon}</p>
    <div className='message-content'>
      <p>{props.title}</p>
      <p>{props.message}</p>
    </div>
  </div>
);