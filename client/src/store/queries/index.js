import { gql } from 'apollo-boost';

export const GET_ALL_CONTRACTS = gql`
  query{
    getAllContracts {
      _id
      title
      initialDate
      dueDate
      file
      createdDate
      user
    }
  }
`;

export const REGISTER_CONTRACT = gql`
  mutation($title:String!, $initialDate:String!,$dueDate:String!, $file: String!, $user: String) {
    addContract(title: $title, initialDate: $initialDate, dueDate: $dueDate, file: $file, user: $user ){
      title,
      initialDate,
      dueDate,
      file,
      user
    }
  }
`;

export const DELETE_CONTRACT = gql`
  mutation($_id: ID!){
    deleteContract(_id: $_id){
      _id
    }
  }
`;

export const GET_ALL_USERS = gql`
  query{
    getAllUsers {
      _id
      name
      lastname
      email
      cpf
      phone
    }
  }
`;

export const REGISTER_USER = gql`
  mutation(
    $name: String!, 
    $lastname: String!,
    $email: String!,
    $cpf: String!,
    $phone: String!
    ){
    registerUser(
      name:$name, 
      lastname:$lastname, 
      email: $email, 
      cpf:$cpf, 
      phone: $phone)
    {
      name
      lastname
      email
      cpf
      phone
    }
  }
`;